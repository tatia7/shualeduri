package com.example.shualeduriapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FilmsViewModel : ViewModel() {

    private val filmsLiveData = MutableLiveData<List<Items>>().apply {
        mutableListOf<Items>()
    }
    val _filmsLiveData: LiveData<List<Items>> = filmsLiveData


    private val loadingLiveData = MutableLiveData<Boolean>()
    val _loadingLiveData : LiveData<Boolean> = loadingLiveData
    fun init(){
        CoroutineScope(Dispatchers.IO).launch {
            getCountries()
        }
    }

    private suspend fun getCountries() : MutableList<Items>?{

        val result  = RetrofitService.retrofitService().getCountry()
        if (result.isSuccessful){
            val items = result.body()
            filmsLiveData.postValue(items)
            return items?.toMutableList()
        }else{
            return null
        }
        loadingLiveData.postValue(false)
    }
}