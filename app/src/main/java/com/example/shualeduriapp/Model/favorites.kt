package com.example.shualeduriapp.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "favorites")
data class favorites (
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo val name : String)