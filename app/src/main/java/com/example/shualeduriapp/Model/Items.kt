package com.example.shualeduriapp.Model

import android.widget.ImageView

data class Items(val title : String? = null,
                 val description :String? = null,
                val release_date: String = "https://cinemania.adjaranet.com/wp-content/uploads/2018/04/0-8-560x375.jpg.svg")
