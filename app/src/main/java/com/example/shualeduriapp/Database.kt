package com.example.shualeduriapp

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [favorites::class], version = 1, exportSchema = false)
abstract class FavDatabase : RoomDatabase(){

    abstract fun userDao(): UserDao

    companion object{
        @Volatile
        private var INSTANCE : FavDatabase? = null

        fun getDatabase(context: Context): FavDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FavDatabase::class.java,
                    "favourites"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}