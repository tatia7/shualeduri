package com.example.shualeduriapp

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shualeduriapp.databinding.AnimeviewBinding

class recViewAdapter : RecyclerView.Adapter<recViewAdapter.viewHolder>() {

    private val films = mutableListOf<Items>()
    private val favorites = mutableListOf<favorites>()

    inner class viewHolder(private val binding: AnimeviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Items
        private lateinit var favs : favorites

        fun bind() {
            model = films[adapterPosition]
            ///loadSvg(model.release_date)
            binding.tvTitle.text = model.title
            binding.description.text = model.description
        }

        fun add(){
            //favs = favorites[adapterPosition]
            if (binding.favButton.isChecked){
                binding.tvTitle.text = favs.name
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        return viewHolder(
            AnimeviewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        holder.bind()
        holder.add()
    }

    override fun getItemCount() = films!!.size

    fun setData(items: MutableList<Items>) {
        this.films.clear()
        this.films.addAll(items)
        notifyDataSetChanged()
    }

}
