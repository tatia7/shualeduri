package com.example.shualeduriapp.WebService

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val URL = "https://ghibliapi.herokuapp.com"

    fun retrofitService(): RetrofitRepository{

        return Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).
                build().create(RetrofitRepository::class.java)
    }
}