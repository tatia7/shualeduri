package com.example.shualeduriapp.WebService

import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepository {
    @GET("films")
    suspend fun getCountry(): Response<List<Items>>
}