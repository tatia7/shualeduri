package com.example.shualeduriapp.Fragment

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shualeduriapp.FilmsViewModel
import com.example.shualeduriapp.R
import com.example.shualeduriapp.databinding.FragmentMainPageBinding
import com.example.shualeduriapp.recViewAdapter
import com.google.android.material.navigation.NavigationView

class MainPageFragment : Fragment() {

    private val films: FilmsViewModel by viewModels()
    private lateinit var binding: FragmentMainPageBinding
    private lateinit var adapter : recViewAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainPageBinding.inflate(layoutInflater, container, false)
        init()
        binding.profilteBtn.setOnClickListener {
            findNavController().navigate(R.id.action_mainPageFragment_to_profileFragment)
        }
        return binding.root
    }
    private fun init(){
        films.init()
        initRec()
        observes()
    }

    private fun observes(){
        films._loadingLiveData.observe(viewLifecycleOwner, {

        })
        films._filmsLiveData.observe(viewLifecycleOwner, {
            adapter.setData(it.toMutableList())
        })
    }
    private fun initRec(){
        adapter = recViewAdapter()
        binding.firstRecView.layoutManager = LinearLayoutManager(this@MainPageFragment.context, LinearLayoutManager.HORIZONTAL, false)
        binding.firstRecView.adapter = adapter
    }



}