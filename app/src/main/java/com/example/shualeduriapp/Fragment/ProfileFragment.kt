package com.example.shualeduriapp.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shualeduriapp.databinding.FragmentProfileBinding
import com.google.firebase.auth.FirebaseAuth
import kotlin.math.log

class ProfileFragment : Fragment() {

    private lateinit var favorites: favorites
    private lateinit var binding: FragmentProfileBinding
    private lateinit var adapter: favoritesAdapter
    private lateinit var auth : FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        auth = FirebaseAuth.getInstance()
        var currentUser = auth.currentUser
        binding.logOut.setOnClickListener {
            if (currentUser != null){
                findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
                Toast.makeText(this@ProfileFragment.context, "Log Out", Toast.LENGTH_SHORT ).show()
            }
        }
        init()
        return binding.root }
    private fun init(){
        adapter = favoritesAdapter()
        binding.secRecView.layoutManager = LinearLayoutManager(this@ProfileFragment.context)
        binding.secRecView.adapter = adapter
    }


}