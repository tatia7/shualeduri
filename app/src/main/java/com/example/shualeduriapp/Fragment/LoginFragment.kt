package com.example.shualeduriapp.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.shualeduriapp.R
import com.example.shualeduriapp.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.initialize

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var auth : FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater, container,false)
        this@LoginFragment.context?.let { Firebase.initialize(it) }
        auth = FirebaseAuth.getInstance()
        binding.regBtn.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        binding.loginBtn.setOnClickListener {
            LoginIn()
        }
        return binding.root
    }
    private fun LoginIn(){
        checkValues()
        if (checkValues()){
            auth.signInWithEmailAndPassword(binding.loginEmail.text.toString(), binding.loginPass.text.toString())
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        val user = auth.currentUser
                        Toast.makeText(requireContext(), "Successful",Toast.LENGTH_SHORT).show()
                        updateUI(user)
                    } else {
                        Toast.makeText(requireContext(), "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                        updateUI(null)
                    }
                }
        }
    }
    private fun checkValues() : Boolean{
        if ((binding.loginEmail.text.toString().isNotEmpty())&&
            (binding.loginPass.text.toString().isNotEmpty() && binding.loginPass.text.length in 8..20)) {
            return true
        }
        return false
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }
    private fun updateUI(currentUser : FirebaseUser?){
        if(currentUser != null){
            Toast.makeText(
                requireContext(),
                "Successful",
                Toast.LENGTH_SHORT
            ).show()
            findNavController().navigate(R.id.action_loginFragment_to_mainPageFragment)
        }else{
            Toast.makeText(
                requireContext(),
                "LoginFailed",
                Toast.LENGTH_SHORT
            ).show()

        }
    }


}