package com.example.shualeduriapp.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.shualeduriapp.R
import com.example.shualeduriapp.databinding.FragmentRegistrationBinding
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_registration.*

class RegistrationFragment : Fragment() {

    private lateinit var binding: FragmentRegistrationBinding

    private lateinit var auth : FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegistrationBinding.inflate(layoutInflater,container, false)

        binding.signUpBtn.setOnClickListener {
            signUpUser()
        }
        return binding.root
    }
    private fun signUpUser(){
        checkValues()
        val email = binding.regEmail.text.toString()
        val username = binding.regUsername.text.toString()
        val pass = binding.regPassword.text.toString()
        auth = FirebaseAuth.getInstance()
        if (checkValues()){
            auth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        val user = auth.currentUser
                        Toast.makeText(requireContext(),"Successful", Toast.LENGTH_SHORT).show()
                        findNavController().navigate(R.id.action_registrationFragment_to_loginFragment)
                    } else {
                        Toast.makeText(requireContext(), "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
    private fun checkValues(): Boolean{
        if ((regUsername.text.toString().isNotEmpty() && regUsername.text.length in 6..20) &&
            (regEmail.text.toString().isNotEmpty())&&
            (regPassword.text.toString().isNotEmpty() && regPassword.text.length in 8..20)) {
            return true
        }
        return false
    }

}

