package com.example.shualeduriapp

import androidx.lifecycle.LiveData

class FavRepository(private val userDao: UserDao) {
    val readAllFav : LiveData<List<favorites>> = userDao.readFavorites()

    suspend fun addFav(items: favorites){
        userDao.addFavorite(items)
    }
}