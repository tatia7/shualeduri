package com.example.shualeduriapp

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFavorite(fav : favorites)

    @Query("SELECT * FROM favorites")
    fun readFavorites() : LiveData<List<favorites>>
}