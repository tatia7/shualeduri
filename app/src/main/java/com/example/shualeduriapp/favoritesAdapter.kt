package com.example.shualeduriapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shualeduriapp.databinding.AnimeviewBinding
import com.example.shualeduriapp.databinding.FavoriteLayotBinding
import com.example.shualeduriapp.databinding.FragmentProfileBinding

class favoritesAdapter : RecyclerView.Adapter<favoritesAdapter.viewHolder>() {

    private val favs = mutableListOf<favorites>()

    inner class viewHolder(private val binding: FavoriteLayotBinding) :
        RecyclerView.ViewHolder(binding.root) {
            private lateinit var model : favorites
            fun bind(){
                model = favs[adapterPosition]
                binding.fav.text = model.name
            }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): favoritesAdapter.viewHolder {
        return viewHolder(
            FavoriteLayotBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: favoritesAdapter.viewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = favs.size
}